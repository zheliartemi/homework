//  У програмуванні цикли потрібні для повторення однакових дій певну кількість разів або до того моменту, поки не буде виконана певна умовa
// цикли можна використовувати для обробки введеного користувачем вводу, коли необхідно продовжувати запитувати користувача, доки не буде введено правильні дані
// Явне приведення типів це коли програміст вказує що хоче перетворити значення одного типу на інший тип. Неявне приведення типів це коли перетворення типу відбувається автоматично
const userInput = prompt("Please enter a number:");
const userNumber = parseInt(userInput);

if (isNaN(userNumber)) {
  console.log("Invalid input");
} else {
  let hasNumbers = false;
  for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
      console.log(i);
      hasNumbers = true;
    }
  }
  if (!hasNumbers) {
    console.log("Sorry, no numbers");
  }
}
