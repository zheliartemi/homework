// процес, за яким рядки або символи, які мають спеціальне значення у синтаксисі мови програмування. Щоб не було не логічних помилок в коді
// Стрілкова функція, Оголошення за допомогою ключового слова function
// це механізм, за допомогою якого js робить доступними для використання змінні та функції,які були оголошені пізніше у коді
function createNewUser() {
    let user = {
      firstName: prompt("Введіть своє ім'я:"),
      lastName: prompt("Введіть своє прізвище:"),
      birthday: prompt("Введіть свою дату народження у форматі dd.mm.yyyy:"),
      getLogin: function () {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();{
      }
      return user;
    },
      getAge: function() {
        let today = new Date();
        let birthDate = new Date(this.birthday);
        let age = today.getFullYear() - birthDate.getFullYear();
        let month = today.getMonth() - birthDate.getMonth();
        if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
          age--;
        }
        return age;
      },
      getPassword: function() {
        let firstLetter = this.firstName.charAt(0).toUpperCase();
        let lastPart = this.lastName.toLowerCase();
        let birthYear = new Date(this.birthday).getFullYear();
        return firstLetter + lastPart + birthYear;
      }
    };
    return user;
  }
  
  let user = createNewUser();
  console.log(user.getLogin());
  console.log("Вік користувача: " + user.getAge());
  console.log("Пароль користувача: " + user.getPassword());