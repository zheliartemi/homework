// 1) Undefined, Null, Boolean, String, Number, Object.
// 2) Оператор == порівнює щоб значення були рівні, тоді як === — порінює на однаковість.
// 3) Оператор оброблює інформацію.



const nameInput = document.getElementById('name');
const ageInput = document.getElementById('age');
const submitButton = document.getElementById('submit');

let name = prompt('Please enter your name:');
let age = parseInt(prompt('Please enter your age:'));

while (!name || isNaN(age)) {
  name = prompt('Please enter your name:');
  age = parseInt(prompt('Please enter your age:'));
}

if (age < 18) {
  alert('You are not allowed to visit this website.');
} else if (age >= 18 && age <= 22) {
  const confirmResult = confirm('Are you sure you want to continue?');
  if (confirmResult) {
    alert('Welcome, ' + name + '!');
  } else {
    alert('You are not allowed to visit this website.');
  }
} else {
  alert('Welcome, ' + name + '!');
}


submitButton.addEventListener('click', function() {
    const name = nameInput.value;
    const age = parseInt(ageInput.value);

    if (validateInput(name, age)) {
        showAgeVerificationMessage(name, age);
    }
});
