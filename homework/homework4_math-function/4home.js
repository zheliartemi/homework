// дозволяє розбивати складні завдання на більш дрібні і прості
// Передавання аргументів у функції дозволяє забезпечити більшу гнучкість та повторне використання коду
// Коли виконується оператор return, виконання функції припиняється і виходить з неї з поверненням вказаного значення


let num1 = prompt("Введіть перше число:");
let num2 = prompt("Введіть друге число:");
let operator = prompt("Введіть математичну операцію (+, -, *, /):");

function calculate(num1, num2, operator) {
  switch (operator) {
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "*":
      return num1 * num2;
    case "/":
      return num1 / num2;
    default:
      return "Неправильна операція";
  }
}

console.log(calculate(Number(num1), Number(num2), operator));
